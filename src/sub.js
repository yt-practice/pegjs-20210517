const r = require('./gen/lang')
/**
 * @param {string} code
 * @return {{ is: 'ok', data: string[][] } | { is: 'err', err: * }}
 */
const parsecsv = code => {
	try {
		return { is: 'ok', data: r.parse(code) }
	} catch (err) {
		return { is: 'err', err }
	}
}
const list = [
	`hoge,gheog,hoge
huga,"pi
yo",iff`,
	`hgie`,
]
list.map(code => {
	console.log(parsecsv(code))
})
