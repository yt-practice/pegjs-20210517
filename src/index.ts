import * as fs from 'fs/promises'
import * as pt from 'path'
import { $ } from './exec'

export const main = async () => {
	await $`yarn rimraf src/gen`
	await $`mkdir -p src/gen`
	await fs.writeFile(pt.join(__dirname, '../src/gen/lang.d.ts'), deco)
	await $`yarn pegjs -o src/gen/lang.js peg/lang.pegjs`
	await $`yarn node src/sub.js`
}

const deco = `interface ParserOptions {
	startRule?: string;
	tracer?: any;
	[key: string]: any;
}
export class SyntaxError extends Error {name: 'SyntaxError'}
export const parse: (input:string, options?:ParserOptions)=>any
`

main().catch(x => {
	console.log('# something happens.')
	console.error(x)
	if ('undefined' === typeof process) return
	process.exit(1)
})
