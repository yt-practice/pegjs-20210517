import * as cp from 'child_process'
import * as pt from 'path'

export const exec = (code: string) =>
	new Promise<string>((ok, fail) => {
		cp.exec(
			code,
			{ cwd: pt.join(__dirname, '..') },
			(error, stdout, stderr) => {
				if (stderr) console.error(stderr)
				if (error) return fail(error)
				ok(stdout)
			},
		)
	})

export const $ = async ([code]: readonly string[]) => {
	if (!code) return
	const [file, ...rest] = code.split(/ /giu)
	if (!file) return
	await new Promise<void>((ok, fail) => {
		const p = cp.spawn(file, rest, { cwd: pt.join(__dirname, '..') })
		p.stdout.on('data', mes => console.log(String(mes).trimEnd()))
		p.stderr.on('data', mes => console.error(String(mes).trimEnd()))
		p.on('close', ok)
		p.on('error', x => fail(x))
	})
}
