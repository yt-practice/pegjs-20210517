start = Lines

Lines = l:Line l2:NLine* NL? { return [l].concat(l2) }

NLine = NL l:Line { return l }

Line = s:String p:Part* { return [s].concat(p) }

Part = Comma s:String { return s }

String =
	"\"" chars:Char* "\"" { return chars.join('') }
	/ $ [^,"\r\n]*

Char
	= $ [^"]
	/ "\"\"" { return '"' }

Comma = ","

NL "line feed"
	= "\r\n" / "\n" / "\r"
